\select@language {ngerman}
\contentsline {section}{\nonumberline Abbildungsverzeichnis}{II}{section*.5}
\contentsline {section}{\nonumberline Tabellenverzeichnis}{II}{section*.7}
\contentsline {section}{Abk\IeC {\"u}rzungsverzeichnis}{III}{section*.8}
\contentsline {section}{\numberline {1}Einf\IeC {\"u}hrung}{1}{section.9}
\contentsline {subsection}{\numberline {1.1}Projektumfeld}{1}{subsection.13}
\contentsline {subsection}{\numberline {1.2}Projektziel}{1}{subsection.14}
\contentsline {subsection}{\numberline {1.3}Projektbegr\IeC {\"u}ndung}{2}{subsection.18}
\contentsline {subsection}{\numberline {1.4}Projektabgrenzung}{3}{subsection.20}
\contentsline {section}{\numberline {2}Ist-Analyse}{3}{section.21}
\contentsline {section}{\numberline {3}Anforderungen}{4}{section.28}
\contentsline {section}{\numberline {4}Nutzwertanalyse}{5}{section.33}
\contentsline {subsection}{\numberline {4.1}Nutzen}{5}{subsection.34}
\contentsline {subsection}{\numberline {4.2}Kosten}{7}{subsection.38}
\contentsline {section}{\numberline {5}Konzept}{7}{section.41}
\contentsline {section}{\numberline {6}Implementierung}{9}{section.48}
\contentsline {section}{\numberline {7}Integration}{10}{section.63}
\contentsline {section}{\numberline {8}Tests}{12}{section.79}
\contentsline {section}{\numberline {9}Auswertung}{13}{section.85}
\contentsline {subsection}{\numberline {9.1}Soll-Ist-Abgleich}{14}{subsection.86}
\contentsline {section}{\numberline {10}\IeC {\"U}bergabe}{14}{section.87}
\contentsline {section}{\numberline {11}Fazit}{14}{section.88}
\contentsline {section}{\numberline {A}Glossar}{i}{appendix.89}
\contentsline {section}{\numberline {B}Anhang}{iii}{appendix.90}
\contentsline {subsection}{\numberline {B.1}Abweichungen vom Zeitplan}{iii}{subsection.91}
\contentsline {subsection}{\numberline {B.2}Betriebshandbuch}{iii}{subsection.94}
\contentsline {subsubsection}{\numberline {B.2.1}Runner}{iii}{subsubsection.95}
\contentsline {subsubsection}{\numberline {B.2.2}Kubernetes-Integration}{v}{subsubsection.100}
\contentsline {subsection}{\numberline {B.3}deployment.yml}{vi}{subsection.103}
\contentsline {subsection}{\numberline {B.4}Dockerfile}{vi}{subsection.104}
\contentsline {subsection}{\numberline {B.5}Gitlab-ci Pipeline-Schema}{vii}{subsection.105}
\contentsline {subsection}{\numberline {B.6}gitlab-ci.yml}{viii}{subsection.108}
\contentsline {subsection}{\numberline {B.7}Kosten Cloud-Anbieter}{viii}{subsection.109}
\contentsline {subsection}{\numberline {B.8}Legende Nutzwertanalyse}{xi}{subsection.116}
\contentsline {subsection}{\numberline {B.9}MySQL-Deployment}{xi}{subsection.119}
\contentsline {subsection}{\numberline {B.10}Service f\IeC {\"u}r Anwendung}{xi}{subsection.120}
\contentsline {subsection}{\numberline {B.11}\IeC {\"U}bersicht Kubernetes Cluster}{xii}{subsection.121}
\contentsline {subsection}{\numberline {B.12}Workflow}{xiii}{subsection.124}
